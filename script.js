const arrInfo = [];
const arrText = [];
const arrHeader = [];

fetch('info.json')
.then(el=>el.json())
.then(el=>{
    el.info.forEach(element => arrInfo.push(element));
    el.title.forEach(element => arrText.push(element));
    el.header.forEach(element => arrHeader.push(element));
})
.catch(error=>console.error(error));

$('.products-box').click(event => {
    $('#jsTitleProducts').html(arrText[event.target.id]);
    $('#jsContentProducts').html(arrInfo[event.target.id]);
});

$('.navbar-brand').click(() => {
    $('#jsTitleProducts').html('Excello este o ferma care se coupa de chestii.');
    $('#jsContentProducts').html('Ne place laptele');
});

$('.nav').click(event => {
    $('.jsHeader').text(arrHeader[event.target.id])
})

$('.navbar-toggler').click(() => {
    $('.jsHeader').empty();
})

